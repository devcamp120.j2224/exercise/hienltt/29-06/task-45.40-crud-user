/*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
    // Biến toàn cục để lưu trữ id user đang đc update or delete;
    var gUserID = "";

    var gCountryObjects = [
        {
            value: 'VN',
            text: 'Việt Nam'
        },
        {
            value: 'USA',
            text: 'USA'
        },
        {
            value: 'AUS',
            text: 'Australia'
        },
        {
            value: 'CAN',
            text: 'Canada'
        }
    ];
    //Khai báo biến mảng hằng số chứa danh sách tên các thuộc tính
    const gUSER_COLS = ['id', 'firstname', 'lastname', 'country','subject', 'customerType', 'registerStatus', 'action'];
    //Biến mảng toàn cục định nghĩa chỉ số các cột tương ứng
    const gUSER_ID_COL = 0;
    const gUSER_FIRSTNAME_COL = 1;
    const gUSER_LASTNAME_COL = 2;
    const gUSER_COUNTRY_COL = 3;
    const gUSER_SUBJECT_COL = 4;
    const gUSER_CUSTOMER_TYPE_COL = 5;
    const gUSER_REGISTER_STATUS_COL = 6;
    const gUSER_ACTION_COL = 7;
    // Đổ dữ liệu vào bảng và thực hiện nút Sửa, Xoá
    $('#user-table').DataTable({
        columns: [
            {data: gUSER_COLS[gUSER_ID_COL]},
            {data: gUSER_COLS[gUSER_FIRSTNAME_COL]},
            {data: gUSER_COLS[gUSER_LASTNAME_COL]},
            {data: gUSER_COLS[gUSER_COUNTRY_COL]},
            {data: gUSER_COLS[gUSER_SUBJECT_COL]},
            {data: gUSER_COLS[gUSER_CUSTOMER_TYPE_COL]},
            {data: gUSER_COLS[gUSER_REGISTER_STATUS_COL]},
            {data: gUSER_COLS[gUSER_ACTION_COL]},
        ],
        columnDefs: [
            {   //Định nghĩa lại cột Country
                targets: gUSER_COUNTRY_COL,
                render: getCountryTextByCountryValue
            },
            {   //Định nghĩa lại cột Action
                targets: gUSER_ACTION_COL,
                className: 'text-center',
                defaultContent: `<button class="btn btn-primary btn-edit">Sửa</button>
                                <button class='btn btn-danger btn-delete' data-toggle='modal'>Xoá</button>`
            }
        ],
        autoWidth: false
    });
    //Khai báo biến mảng chứa dữ liệu users
    var gListUser = [];
    /*** REGION 2 - Vùng gán / thực thi sự kiện cho các elements */
    $(document).ready(function(){
        onPageLoading();
    });
    // 1 - C: Gán event handler cho sự kiện Create
    $('#btn-add-user').on('click', onBtnAddNewUserClick);
    // 3 - U: Gán event handler cho nút Sửa
    $('#user-table').on('click', '.btn-edit', onBtnEditClick);
    // 4 - D: Gán event handler cho nút Xoá
    $('#user-table').on('click', '.btn-delete', onBtnDeleteClick);
    // Gán event handler cho nút Insert user trên modal insert
    $('#btn-create-user').on('click', onBtnCreatUserClick);
    // Gán event handler cho nút Update user trên modal update
    $('#btn-update-voucher').on('click', onBtnUpdateUserClick);
    // Gán event handler cho nút Confirm delete trên modal delete user
    $('#btn-confirm-delete-user').on('click', onBtnConfirmDeleteUserClick);

    /*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
    //hàm thực thi khi load trang
    function onPageLoading(){
        "use strict";
        getAllUsers();
        // 2 - R: Load user to DataTable
        loadDataToUserTable(gListUser);
    }
    //Hàm gọi khi nút Sửa được click
    function onBtnEditClick(){
        "use strict";
        console.log("Nút Sửa được click");
        //gán Id user cho biến toàn cục
        gUserID = getUserIdFromButton(this);
        //Gọi API để lấy data user theo userid và hiện lên form modal
        $.ajax({
            url: "http://42.115.221.44:8080/crud-api/users/" + gUserID,
            type: 'GET',
            dataType: 'json',
            success: function(res){
                console.log(res);
                showUserDataToModalUpdate(res);
            },
            error: function(error){
                alert(error.responseText);
            }
        });
        //hiển thị modal lên
        $('#update-user-modal').modal('show');
    }
    //Hàm thực thi khi nhấn nút Xoá
    function onBtnDeleteClick(){
        "use strict";
        console.log("Nút Xoá được click");
        //gán Id user cho biến toàn cục
        gUserID = getUserIdFromButton(this);
        //hiển thị modal lên
        $('#delete-confirm-modal').modal('show');
    }
    //Hàm thực thi khi nhấn nút Thêm user
    function onBtnAddNewUserClick(){
        "use strict";
        $('#insert-user-modal').modal('show');
    }
    //Hàm thực thi khi nhấn nút Insert user trên modal insert
    function onBtnCreatUserClick(){
        "use strict";
        //Khai báo đối tượgn chứa user
        var vUserObj = {
            firstname: "",
            lastname: "",
            subject: "",
            country: "",
        };
        //B1: thu thập dữ liệu
        getUserDataCreate(vUserObj);
        //B2: validate
        var vCheck = validateInsertData(vUserObj);
        if(vCheck){
            //B3: insert user
            postUserApi(vUserObj);
            //B4: xử lý front-end
            //Gọi APi lấy danh sách user
            getAllUsers();
            //Load lại vào bảng dataTable
            loadDataToUserTable(gListUser);
            //Xoá trắng dữ liệu trên modal
            resetCreateUserForm();
            //Ẩn modal insert
            $('#insert-user-modal').modal('hide');
        }
    }
    //Hàm thực thi khi nút Update user được nhấn
    function onBtnUpdateUserClick(){
        "use strict";
        // khai báo đối tượng chứa dữ liệu
        var vUserObj = {
            firstname: "",
            lastname: "",
            subject: "",
            country: "",
            customerType: "",
            registerStatus: "",
        };
        // B1: Thu thập dữ liệu
        getDataFromModalUpdateToUpdateUser(vUserObj);
        // B2: Validate update
        var vCheck = validateDataToUpdate(vUserObj);
        console.log(vCheck);
        if(vCheck){
            //B3: Gọi API to update user
            var vJsonStringUser = JSON.stringify(vUserObj);
            $.ajax({
                url: "http://42.115.221.44:8080/crud-api/users/" + gUserID,
                type: "PUT",
                contentType: 'application/json; charset=utf-8',
                data: vJsonStringUser,
                success: function(res){
                    console.log(res);
                    alert("Cập nhật user:\n" + vJsonStringUser + " thành công!");
                    //B4: xử lý front-end
                    getAllUsers(); //gọi hàm api lấy lại all user
                    loadDataToUserTable(gListUser); //load lại dữ liệu mới lên bảng
                    resertUpdateUserForm(); //reset lại form modal update
                    $('#update-user-modal').modal('hide'); //ẩn modal update
                },
                error: function(error){
                    alert(error.responseText);
                }
            });
        }
        
    }
    //Hàm thực thi khi nhấn nút Confirm delete user
    function onBtnConfirmDeleteUserClick(){
        "use strict";
        // B1: thu thập dữ liệu (ko có)
        // B2: validate (ko có)
        // B3: Gọi API để Delete user theo id
        $.ajax({
            url: "http://42.115.221.44:8080/crud-api/users/" + gUserID,
            type: 'DELETE',
            success: function(res){
                console.log(res);
                alert("Đã xoá user có ID " + gUserID + " thành công!");
                //B4: xử lý front-end
                getAllUsers(); //gọi hàm api lấy lại all user
                loadDataToUserTable(gListUser); //load lại dữ liệu mới lên bảng
                // ẩn modal đi
                $("#delete-confirm-modal").modal("hide");
            },
            error: function(error){
                alert(error.responseText);
            }
        });
    }
    /*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
    // hàm render từ value country sang text country
    function getCountryTextByCountryValue(paramCountry){
        "use strict";
        var vTextCountry = "";
        for (var bI = 0; bI < gCountryObjects.length; bI++){
            if(gCountryObjects[bI].value == paramCountry || paramCountry == gCountryObjects[bI].text){
                vTextCountry = gCountryObjects[bI].text
            }
        };
        return vTextCountry;
    }
    // hàm gọi API lấy danh sách users
    function getAllUsers(){
        "use strict";
        $.ajax({
            async: false,
            url: 'http://42.115.221.44:8080/crud-api/users/',
            type: 'GET',
            dataType: 'json',
            success: function(res){
                //Lấy response trả về và gán cho bến toàn cục
                gListUser = res;
            },
            error: function(error){
                alert(error.responseText);
            }
        });
    }
    // Hàm load users vào dataTable
    function loadDataToUserTable(paramObjUsers){
        "use strict";
        console.log(paramObjUsers);
        var vTable = $('#user-table').DataTable();
        vTable.clear();
        vTable.rows.add(paramObjUsers);
        vTable.draw();
    }
    // hàm thu thập dữ liệu để insert user
    function getUserDataCreate(paramUserObj){
        "use strict";
        paramUserObj.firstname = $('#inp-firstname').val().trim();
        paramUserObj.lastname = $('#inp-lastname').val().trim();
        paramUserObj.subject = $('#inp-subject').val().trim();
        paramUserObj.country = $('#select-country').val();
    }
    //Hàm validate data khi insert
    function  validateInsertData(paramObjUsers){
        "use strict";
        if(paramObjUsers.firstname == ""){
            alert("Hãy nhập Firstname!");
            return false;
        }
        else if (paramObjUsers.lastname == ""){
            alert("Hãy nhập Lastname!");
            return false;
        }
        else if (paramObjUsers.subject == ""){
            alert("Hãy nhập Subject!");
            return false;
        }
        else if (paramObjUsers.country == 'NOT_SELECT_COUNTRY'){
            alert("Hãy chọn Country!");
            return false;
        }
        return true;
    }
    //hàm gọi API để insert user
    function  postUserApi(paramObjUsers){
        "use strict";
        $.ajax({
            async: false,
            url: "http://42.115.221.44:8080/crud-api/users/",
            type: 'POST',
            data: JSON.stringify(paramObjUsers),
            contentType: 'application/json',
            success: function(res){
                console.log(res);
                alert("Thêm user thành công!");
            },
            error: function(error){
                alert(error.responseText);
            }
        });
    }
    //Hàm xoá trắng form insert user
    function resetCreateUserForm(){
        "use strict";
        $('#inp-firstname').val("");
        $('#inp-lastname').val("");
        $('#inp-subject').val("");
        $('#select-country').val("NOT_SELECT_COUNTRY");
    }
    // hàm dựa vào button detail (edit or delete) xác định đc id voucher
    function getUserIdFromButton(paramButton) {
        "use strict";
        var vRowClick = $(paramButton).closest('tr'); // Xác định tr chứa nút được click
        var vTable = $('#user-table').DataTable(); //TRuy xuất đến dataTable
        var vDataRow = vTable.row(vRowClick).data(); //Lấy dữ liệu của hàng chứa nút được click
        console.log("ID của user tương ứng: " + vDataRow.id);
        return vDataRow.id;
    }
    // hàm hiển thị 1 data user lên modal update
    function showUserDataToModalUpdate(paramResponse){
        "use strict";
        $('#inp-firstname-update-modal').val(paramResponse.firstname);
        $('#inp-lastname-update-modal').val(paramResponse.lastname);
        $('#inp-subject-update-modal').val(paramResponse.subject);
        $('#select-country-update-modal').val(paramResponse.country);
        $('#select-customer-type-update-modal').val(paramResponse.customerType);
        $('#select-register-status-update-modal').val(paramResponse.registerStatus);
    }
    // hàm thu thập dữ liệu trên modal update 
    function getDataFromModalUpdateToUpdateUser(paramUserObj) {
        "use strict";
        paramUserObj.firstname = $("#inp-firstname-update-modal").val().trim();
        paramUserObj.lastname = $("#inp-lastname-update-modal").val().trim();
        paramUserObj.subject = $('#inp-subject-update-modal').val().trim();
        paramUserObj.country = $('#select-country-update-modal').val();
        paramUserObj.customerType = $('#select-customer-type-update-modal').val();
        paramUserObj.registerStatus = $('#select-register-status-update-modal').val();
    }
    // hàm validate data để update user
    function validateDataToUpdate(paramUserObj) {
        "use strict";
        if(paramUserObj.firstname == "") {   
            alert("Cần nhập Firstname");
            return false;
        }
        else if(paramUserObj.lastname == ""){
            alert("Cần nhập Lastname");
            return false;
        }
        else if(paramUserObj.country == "NOT_SELECT_COUNTRY" || paramUserObj.country == null){
            alert("Hãy chọn Country");
            return false;
        }
        else if(paramUserObj.customerType == "NOT_SELECT_CUSTOMER_TYPE" || paramUserObj.customerType == null){
            alert("Hãy chọn Customer Type");
            return false;
        }
        else if(paramUserObj.registerStatus == "NOT_SELECT_REGISTER_STATUS" || paramUserObj.registerStatus == null){
            alert("Hãy chọn Register Status");
            return false;
        }
        return true;
    }
    // hàm xóa trắng form update user trên modal
    function resertUpdateUserForm() {
        "use strict";
        $("#inp-firstname-update-modal").val("");
        $("#inp-lastname-update-modal").val("");
        $("#inp-subject-update-modal").val("");
        $("#select-country-update-modal").val("NOT_SELECT_COUNTRY");
        $("#select-customer-type-update-modal").val("NOT_SELECT_CUSTOMER_TYPE");
        $("#select-register-status-update-modal").val("NOT_SELECT_REGISTER_STATUS");
    } 